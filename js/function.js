//---------------Gestion des clics sur les liens ayant la classe .treeview---------------//
	$(document).ready(function(){
		//au click sur le lien dans le menu
		$("#middle").on("click", ".treeView", function(evt){
			evt.preventDefault(); // annule l'action du lien
			//on recupere la valeur de l'attribut id pour afficher tel ou tel resultat
			$page = ($(this).attr('href'));
			$.ajax({
				url : $page,
				cache : false,
				success : function(html){
					display(html);
				},
				error : function(XMLHttRequest, textStatus, errorThrown){
					alert('Error XMLHttRequest : '+XMLHttRequest);
					alert('Error textStatus : '+textStatus);
					alert('Error errorThrown : '+errorThrown);
				}
			})
			return false;
		});
	});
	
	function display(data){
		$('#content').fadeOut(function(){
			$('#content').empty();
			$('#content').append(data);
			$('#content').fadeIn();
		});
	}
//---------------Gestion des clics sur les liens ayant la classe .treeview---------------//